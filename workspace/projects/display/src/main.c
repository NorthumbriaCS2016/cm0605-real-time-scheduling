/*
 * Simple program to flash the LEDs while sending CAN messages on port 1
 * and receiving them by polling on port 2.
 *
 * Implemented using a time-triggered scheduler.
 *
 * DK - 17-Nov 2010

 * Display Monitor - Peter McNeill
 */


#include <stdbool.h>
#include <stdint.h>
#include <bsp.h>
#include <lcd.h>
#include <leds.h>
#include <can.h>
#include <scheduler.h>


uint32_t txCount;
uint32_t rxCount;

uint32_t requestTemp;


void linkLedToggleTask(void);
void connectLedToggleTask(void);
void RequestT(void);
void DisplayT(void);
void taskRequest(void);


int main () {
  
  bspInit();
  
  schInit();
  schAddTask(RequestT, 0, 500);
  schAddTask(DisplayT, 1, 20);
 


  schStart();
  
  while (true) {
    schDispatch();
  }
}


void RequestT(void) {
  
  static canMessage_t txMsg = {0x23, 4, 0, 0};
  bool txOk;
    
  // Transmit message on CAN 1
  txOk = canWrite(CAN_PORT_1, &txMsg);
  if (txOk) {
    requestTemp = 1;
    txMsg.dataA = requestTemp;
  }
}

void DisplayT(void) {

  static canMessage_t rxMsg;
  
  if (canReady(CAN_PORT_1)) {           
    canRead(CAN_PORT_1, &rxMsg);
    if(rxMsg.dataA){
    lcdSetTextPos(2,2);
    lcdWrite("TEMPERATURE"); 
    lcdSetTextPos(2,3);
    lcdWrite("%03u", rxMsg.dataA);
    }
  }
  
}
