/*
 * The EngineMonitor system is an event-driven system, implemented using a
 * real-time operating system, uC/OS-II.
 * 
 * Display.RequestT periodically transmits a request message to the EngineMonitor 
 * processor. The EngineMonitor.TempT task is released by an interrupt handler that 
 * is triggered by a CAN controller on the arrival of a request message. When a 
 * request message is received, the TempT task reads the local temperature and 
 * sends a reply message containing the temperature value. 
 * 
 * Another periodic task, TachoT, runs on the EngineMonitor processor. It runs at a 
 * higher priority than TempT and may be scheduled to run at the same time. 
 * 
 * The time-triggered task Display.DisplayT polls its CAN controller for the arrival
 * of a message containing the temperature value and, when it receives a message,
 * displays the value.
 * 
*/


#include <stdbool.h>
#include <ucos_ii.h>
#include <bsp.h>
#include <osutils.h>
#include <leds.h>
#include <lcd.h>
#include <can.h>
#include <stdlib.h>  // rand()
#include <buttons.h>
#include <timers.h>


/*
*********************************************************************************************************
*                                            APPLICATION TASK PRIORITIES
*********************************************************************************************************
*/

#define  APP_TASK_TACHO_T_PRIO                  5    
#define  APP_TASK_TEMP_T_PRIO                   10              

/*
*********************************************************************************************************
*                                            APPLICATION TASK STACKS
*********************************************************************************************************
*/

#define  APP_TASK_TACHO_T_STK_SIZE              256
#define  APP_TASK_TEMP_T_STK_SIZE              256

static OS_STK appTaskTempTStk[APP_TASK_TACHO_T_STK_SIZE];
static OS_STK appTaskTachoTStk[APP_TASK_TEMP_T_STK_SIZE];

/*
*********************************************************************************************************
*                                            GLOBAL VARIABLES
*********************************************************************************************************
*/

uint32_t rxCount;                       // counter for CAN message receptions
uint32_t temperature;                   // counter for CAN message transmissions
canMessage_t can1RxBuffer;              // CAN message receive buffer for CAN port 1
canMessage_t can2RxBuffer;              // CAN message receive buffer for CAN port 1
bool can1RxDone;                        // Flag indicating message reception complete on CAN port 1
bool can2RxDone;                        // Flag indicating message reception complete on CAN port 1
 

/*
*********************************************************************************************************
*                                            APPLICATION FUNCTION PROTOTYPES
*********************************************************************************************************
*/

static void appTaskTempT(void *pdata);
static void appTaskTachoT(void *pdata);

/*
*********************************************************************************************************
*                                            LOCAL FUNCTION PROTOTYPES
*********************************************************************************************************
*/

void canHandler(void);                   // interrupt handler for CAN message reception
static OS_EVENT *CanSem;

/*
*********************************************************************************************************
*                                            GLOBAL FUNCTION DEFINITIONS
*********************************************************************************************************
*/

int main() {

  /* Initialise the board support package */
  bspInit();

  /* Initialise the OS */
  OSInit();                                                   

  /* Create the tasks */
   OSTaskCreate(appTaskTempT,                               
               (void *)0,
               (OS_STK *)&appTaskTempTStk[APP_TASK_TEMP_T_STK_SIZE - 1],
               APP_TASK_TEMP_T_PRIO);
  
  OSTaskCreate(appTaskTachoT,                               
           (void *)0,
           (OS_STK *)&appTaskTachoTStk[APP_TASK_TACHO_T_STK_SIZE - 1],
           APP_TASK_TACHO_T_PRIO);

  
  CanSem = OSSemCreate(1);

  /* Start the OS */
  OSStart();                                                  
  
  /* Should never arrive here */ 
  return 0;      
}

/*
*********************************************************************************************************
*                                            APPLICATION TASK DEFINITIONS
*********************************************************************************************************
*/


void appTaskTempT(void *pdata){
  uint8_t i = 0;
  uint8_t osStatus;
  canRxInterrupt(canHandler);    // configure CAN to interrupt on message reception
  uint32_t minTime = 2147483647; //minimum time taken on all iterations
  uint32_t meanTime = 0; //average time on all iterations
  uint32_t maxTime = 0; //maximum time for 1 iteration
  uint32_t totalTime = 0; // time taken for all iterations
  
  startWatch();
  uint32_t timeElapsed = stopWatch();

  while(true){
    
    
    // from after recieve to after send
    // time taken between the two
    //
    
    OSSemPend(CanSem, 0, &osStatus);
    if (can1RxDone) {    
      startWatch();
      i ++;
      can1RxDone = false;
      canMessage_t msg;
      temperature = temperature + 5;
      msg.id = 0x07;  // CAN message id
      msg.len = 4;    // data length 4
      msg.dataA = temperature;
      msg.dataB = 0;
      canWrite(CAN_PORT_1, &msg);
    }

    osStatus = OSSemPost(CanSem);
    
      timeElapsed = stopWatch();
      //if ((timeElapsed < minTime) && (timeElapsed != 0)) {
      //  minTime = timeElapsed;
      //}
      if (timeElapsed > maxTime) {
        maxTime = timeElapsed;
      }
      totalTime += timeElapsed;
      //meanTime = totalTime / rxCount;

      //debug statements
      lcdSetTextPos(1,1);
      lcdWrite("T T: %d", timeElapsed);
      lcdSetTextPos(1,2);
      lcdWrite("Temp: %d", temperature);
      lcdSetTextPos(1,3);
      lcdWrite("################");
      lcdSetTextPos(1,4);
      lcdWrite("Iter: %d", i);
      lcdSetTextPos(1,5);
      lcdWrite("Max : %d", maxTime);
      lcdSetTextPos(1,6);
      lcdWrite("Tot : %d", totalTime);
      lcdSetTextPos(1,7);
      lcdWrite("Ela : %d", timeElapsed);


  }
  
}

static void appTaskTachoT(void *pdata) {
  osStartTick();
  uint32_t tacho_v; 
  uint32_t i = 0;
  
  uint32_t timeElapsed = 0;
  uint32_t minTime = 99999;
  uint32_t meanTime = 0;
  uint32_t maxTime = 0;
  uint32_t totalTime = 0;

  while ( true ) {
    i ++;
    startWatch();
    tacho_v =  rand() % 1000;
    
    
      timeElapsed = stopWatch();
      if (timeElapsed < minTime) {
        minTime = timeElapsed;
      }
      if (timeElapsed > maxTime) {
        maxTime = timeElapsed;
      }
      totalTime += timeElapsed;
      meanTime = totalTime / i;
    
      /*
    //timer
    lcdSetTextPos(1,1);
    lcdWrite("##TACHO T TIMER##");
    lcdSetTextPos(1,2);
    lcdWrite("Iter: %d", i);
    lcdSetTextPos(1,3);
    lcdWrite("MinT: %d", minTime);
    lcdSetTextPos(1,4);
    lcdWrite("Mean: %d", meanTime);
    lcdSetTextPos(1,5);
    lcdWrite("Max : %d", maxTime);
    lcdSetTextPos(1,6);
    lcdWrite("Tot : %d", totalTime);
    */
    OSTimeDlyHMSM(0,0,0,20);

  }

}


void canHandler(void) {
  uint8_t osStatus;

  if (canReady(CAN_PORT_1)) {
    canRead(CAN_PORT_1, &can1RxBuffer);
    can1RxDone = true;
    osStatus = OSSemPost(CanSem);
    rxCount++;
  }
  if (canReady(CAN_PORT_2)) {
    canRead(CAN_PORT_2, &can2RxBuffer);
    can2RxDone = true;
    osStatus = OSSemPost(CanSem);
  }
}